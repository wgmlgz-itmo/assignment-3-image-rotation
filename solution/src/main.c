#include <stdio.h>
#include <stdlib.h>

#include "../include/util.h"

int validate_and_get_angle(const char *arg) {
  int angle_values[] = {0, 90, -90, 180, -180, 270, -270};
  int angle = atoi(arg);

  for (int i = 0; i < 7; ++i) {
    if (angle == angle_values[i]) {
      return angle;
    }
  }

  return -1;
}

void process_arguments(int argc, char *argv[], char **source_image_path,
                       char **transformed_image_path, int *angle) {
  if (argc != 4) {
    fprintf(stderr,
            "Usage: ./image-transformer <source-image> <transformed-image> "
            "<angle>\n");
    exit(1);
  }

  *source_image_path = argv[1];
  *transformed_image_path = argv[2];
  int new_angle = validate_and_get_angle(argv[3]);
  if (new_angle == -1) {
    fprintf(stderr,
            "Error: Invalid angle. Allowed values: 0, 90, -90, 180, -180, 270, "
            "-270\n");
    exit(1);
  }
  *angle = new_angle;
}

int main(int argc, char *argv[]) {
  char *source_image_path;
  char *transformed_image_path;
  int angle;

  process_arguments(argc, argv, &source_image_path, &transformed_image_path,
                    &angle);

  rotate_image(source_image_path, transformed_image_path, angle);
}
