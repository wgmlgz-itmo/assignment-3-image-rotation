#include <stdio.h>
#include <stdlib.h>

#include "../include/bmp.h"
#include "../include/image.h"

void rotate_image(char* source_image_path, char* transformed_image_path,
                  int angle) {
  FILE* source_file = fopen(source_image_path, "rb");
  if (!source_file) {
    fprintf(stderr, "Error opening source image: %s\n", source_image_path);
    exit(1);
  }

  struct image source_image;
  enum read_status read_result = from_bmp(source_file, &source_image);
  fclose(source_file);

  if (read_result != READ_OK) {
    free(source_image.data);
    fprintf(stderr, "Error reading BMP file.\n");
    exit(1);
  }

  int num_rotations = (abs(angle) % 360) / 90;
  if (angle < 0) {
    num_rotations = 4 - num_rotations;
  }

  for (int i = 0; i < num_rotations; i++) {
    struct image rotated_image = rotate(source_image);

    if (!rotated_image.data) {
      fprintf(stderr, "Failed to allocate memory.\n");
      exit(1);
    }

    free(source_image.data);

    source_image = rotated_image;
  }

  FILE* dest_file = fopen(transformed_image_path, "wb");
  if (!dest_file) {
    fprintf(stderr, "Error opening destination image: %s\n",
            transformed_image_path);
    free(source_image.data);
    exit(1);
  }

  enum write_status write_result = to_bmp(dest_file, &source_image);
  fclose(dest_file);

  if (write_result != WRITE_OK) {
    fprintf(stderr, "Error writing BMP file.\n");
    free(source_image.data);
    exit(1);
  }

  free(source_image.data);
  exit(0);
}
