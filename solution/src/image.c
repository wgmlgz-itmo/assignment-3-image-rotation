
#include "../include/image.h"

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

struct image rotate(struct image const source) {
  struct image rotated;
  rotated.width = source.height;
  rotated.height = source.width;
  rotated.data = (struct pixel*)malloc(sizeof(struct pixel) * rotated.width *
                                       rotated.height);

  if (!rotated.data) {
    return rotated; // handled by caller
  }

  for (uint64_t x = 0; x < source.width; x++) {
    for (uint64_t y = 0; y < source.height; y++) {
      struct pixel src_pixel = source.data[y * source.width + x];

      uint64_t new_x = y;
      uint64_t new_y = rotated.height - x - 1;

      rotated.data[new_y * rotated.width + new_x] = src_pixel;
    }
  }

  return rotated;
}
